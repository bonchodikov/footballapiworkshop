﻿using AutoMapper;
using FootballData;
using FootballData.Contracts;
using FootballData.Entities;
using FootBallServices.Contracts;
using FootBallServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FootBallServices.Services
{
    public class TeamService : ITeamServices
    {
        Data data;
        public TeamService(Data data)
        {
            this.data = data;
        }
        public TeamDTO Create(TeamDTO model, IMapper mapper)
        {
            if (data.Teams.Any(p => p.Name.Equals(model.Name)))
            {
                return null;
            }
            //how to avoid this coupled instanse of playe>? IPlayer or?
            //Is using mapper okay in this situation>? 
            //==> model = mapper.Map<Team>(model)
            var team = new Team();
            team.Name = model.Name;
            data.Teams.Add(team);
            data.SaveChanges();
            return mapper.Map<TeamDTO>(team);
        }

        public ICollection<TeamDTO> GetAll(IMapper mapper)
        {
            var teams = data.Teams.ToArray();
            var result = mapper.Map<ICollection<TeamDTO>>(teams);

            //how to do it with linq instead?!
            foreach (var item in result)
            {
                item.Players = data.Players.Count(c => c.TeamId.Equals(item.Id));
            }
            return result;
        }

        public TeamDTO GetById(int id, IMapper mapper)
        {
            var teams = data.Teams.FirstOrDefault(p => p.Id.Equals(id));

            var result = mapper.Map<TeamDTO>(teams);

            result.Players = data.Players.Count(c => c.TeamId.Equals(result.Id));

            return result;
        }

        public ICollection<TeamDTO> GetByMinPlayers(int minPlayersCountInATeam, IMapper mapper)
        {
            var teams = data.Teams.ToList();

            var results = mapper.Map<ICollection<TeamDTO>>(teams);

            foreach (var item in results)
            {
                item.Players = data.Players.Count(c => c.TeamId.Equals(item.Id));
                item.Scored = data.Players.Where(p => p.TeamId.Equals(item.Id)).Sum(p => p.Goals);
            }
            var final = results.Where(p => p.Players >= minPlayersCountInATeam).ToList();
            return final;
        }

        public ICollection<TeamDTO> GetByName(string name, IMapper mapper)
        {
            var teams = data.Teams.Where(p => p.Name.StartsWith(name));

            var results = mapper.Map<ICollection<TeamDTO>>(teams);

            foreach (var item in results)
            {
                item.Players = data.Players.Count(c => c.TeamId.Equals(item.Id));
                item.Scored = data.Players.Where(p => p.TeamId.Equals(item.Id)).Sum(p => p.Goals);
            }          

            return results;
        }

        public ICollection<TeamDTO> GetByNameAndId(string name,int minPlayersInTeam, IMapper mapper)
        {

            //What's wrong with that query!?...........
            //var teams = data.Teams.Where(p => p.Name.StartsWith(name)
            //&& mapper.Map<TeamDTO>(new Tuple<Team, Data>(p, data)).Players >= minPlayersInTeam).ToList();
            //if (teams.Count > 0)
            //{
            //    var res = mapper.Map<ICollection<TeamDTO>>(teams);
            //    foreach (var item in res)
            //    {
            //        item.Players = data.Players.Count(p => p.TeamId.Equals(item.Id));
            //        item.Scored = data.Players.Where(p => p.TeamId.Equals(item.Id)).Sum(p => p.Goals);
            //    }

            //    return res;
            //}
            //return null;
            var teams = data.Teams.Where(p => p.Name.StartsWith(name)).ToList();
            if (teams == null)
            {
                return null;
            }

            var res = mapper.Map<ICollection<TeamDTO>>(teams);
            foreach (var item in res)
            {
                item.Players = data.Players.Count(p => p.TeamId.Equals(item.Id));
                item.Scored = data.Players.Where(p => p.TeamId.Equals(item.Id)).Sum(p => p.Goals);
            }

            var final = res.Where(p => p.Players > minPlayersInTeam).ToList();

            return final;
        }
    }
}
