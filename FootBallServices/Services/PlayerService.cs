﻿using AutoMapper;
using FootballData;
using FootballData.Entities;
using FootBallServices.Contracts;
using FootBallServices.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FootBallServices.Services
{
    public class PlayerService : IPlayerServices
    {
        Data data;
        public PlayerService(Data data, IMapper mapper)
        {
            this.data = data;
        }
        public PlayerDTO Create(CreatePlayerDTO model, IMapper mapper)
        {
            var res = new Player();
            res.FirstName = model.FirstName;
            res.LastName = model.LastName;
            res.Goals = model.Goals;
            if (model.TeamId != null)
            {
                if (!data.Teams.Any(p => p.Id.Equals(model.TeamId)))
                {
                    return null;
                }
                res.TeamId = model.TeamId;
            }
            var tup = new Tuple<Player, Data>(res, data);
            var rest = mapper.Map<PlayerDTO>(tup);
            data.Players.Add(res);
            data.SaveChanges();
            return rest;
        }

        public ICollection<PlayerDTO> GetAll(IMapper mapper)
        {
            var res = data.Players.ToList();
            var hold = mapper.Map<ICollection<PlayerDTO>>(res).ToList();

            InATeamConfig(res, hold);

            return hold;
        }

        //How to avoid that?!Without using the same come over and over again? 
        private void InATeamConfig(List<Player> res, List<PlayerDTO> hold)
        {
            for (int r = 0; r < res.Count; r++)
            {
                try
                {
                    if (res[r].TeamId != null)
                    {
                        hold[r].TeamName = data.Teams.FirstOrDefault(p => p.Id.Equals(res[r].TeamId)).Name;
                    }
                    continue;
                }
                catch (Exception)
                {

                    hold[r].TeamName = null;
                }

            }
        }

        public ICollection<PlayerDTO> GetByFullName(string firstName, string lastName, IMapper mapper)
        {
            var res = data.Players.Where(p => p.FirstName.Equals(firstName) && p.LastName.Equals(lastName)).ToList();
            var map = mapper.Map<ICollection<PlayerDTO>>(res).ToList();
            InATeamConfig(res, map);
            return map;
        }

        public PlayerDTO GetById(int id, IMapper mapper)
        {
            var res = data.Players.FirstOrDefault(p => p.Id.Equals(id));
            return mapper.Map<PlayerDTO>(new Tuple<Player, Data>(res, data));
        }

        public ICollection<PlayerDTO> GetByFirstName(string name, IMapper mapper)
        {
            var res = data.Players.Where(p => p.FirstName.Equals(name)).ToList();
            var map = mapper.Map<ICollection<PlayerDTO>>(res).ToList();
            InATeamConfig(res, map);
            return map;
        }

        public ICollection<PlayerDTO> GetByLastName(string name, IMapper mapper)
        {
            var res = data.Players.Where(p => p.LastName.Equals(name)).ToList();
            var map = mapper.Map<ICollection<PlayerDTO>>(res).ToList();
            InATeamConfig(res, map);
            return map;
        }
    }
}
