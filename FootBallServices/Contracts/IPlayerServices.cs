﻿using AutoMapper;
using FootBallServices.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace FootBallServices.Contracts
{
    public interface IPlayerServices
    {
        ICollection<PlayerDTO> GetAll(IMapper mapper);
        PlayerDTO GetById(int id, IMapper mapper);
        PlayerDTO Create(CreatePlayerDTO model, IMapper mapper);
        ICollection<PlayerDTO> GetByFirstName(string name, IMapper mapper);
        ICollection<PlayerDTO> GetByLastName(string name, IMapper mapper);
        ICollection<PlayerDTO> GetByFullName(string firstName, string lastName, IMapper mapper);


    }
}
