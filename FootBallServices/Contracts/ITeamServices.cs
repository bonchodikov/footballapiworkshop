﻿using AutoMapper;
using FootBallServices.Models;
using System.Collections.Generic;

namespace FootBallServices.Contracts
{
    public interface ITeamServices
    {
        ICollection<TeamDTO> GetAll(IMapper mapper);
        TeamDTO GetById(int id, IMapper mapper);
        TeamDTO Create(TeamDTO model, IMapper mapper);
        ICollection<TeamDTO> GetByName(string name, IMapper mapper);
        ICollection<TeamDTO> GetByNameAndId(string name, int minPlayersCountInATeam ,IMapper mapper);
        ICollection<TeamDTO> GetByMinPlayers(int minPlayersCountInATeam, IMapper mapper);

    }
}
