﻿namespace FootBallServices.Models
{
    public class TeamDTO
    {        
        public int Id { get; set; }
        public string Name { get; set; }
        public int Players { get; set; }
        public int Scored { get; set; }

    }
}
