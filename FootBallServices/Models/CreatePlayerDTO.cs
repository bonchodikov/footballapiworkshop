﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootBallServices.Models
{
    public class CreatePlayerDTO
    {
        public int Goals { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? TeamId { get; set; }
    }
}
