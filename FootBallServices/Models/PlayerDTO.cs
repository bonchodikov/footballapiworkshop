﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootBallServices.Models
{
    public class PlayerDTO
    {       
        public int Id { get; set; }
        public int Goals { get; set; }
        public string FullName { get; set; }       
        public string TeamName { get; set; }

        public override string ToString()
        {
            var result = new StringBuilder();
            result.AppendLine("player_id: " + Id);
            result.AppendLine("name: " + FullName);
            result.AppendLine("scored: " + Goals);
            if (TeamName!=null)
            {
                result.AppendLine($"team: {TeamName}");
            }
            return result.ToString();
        }
    }
}
