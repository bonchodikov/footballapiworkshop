﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FootBallServices.Contracts;
using FootBallServices.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WorkshopFootballSystem.ViewModels;

namespace WorkshopFootballSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TeamController : ControllerBase
    {
        IMapper mapper;
        ITeamServices service;
        public TeamController(IMapper mapper, ITeamServices services)
        {
            this.mapper = mapper;
            this.service = services;
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            var res = service.GetById(id, mapper);
            if (res!=null)
            {
                return Ok(res);
            }
            return BadRequest("No team found with this Id");
        }

        [HttpGet]
        public IActionResult GetAll()
        {
            var res = service.GetAll(mapper);
            if (res!=null)
            {              
                return Ok(res);
            }
            return Ok("No teams available");
        }
        
        [HttpGet]
        [Route("search")]
        public IActionResult Search([FromQuery]string teamName, [FromQuery]int minPlayers=-1)
        {
            if (teamName!=null)
            {
                if (minPlayers>=0)
                {                   
                    var res = service.GetByNameAndId(teamName, minPlayers, mapper);
                    if (res.Count==0)
                    {
                        return Ok("No result found!");
                    }
                    return Ok(res);
                }
                var res1 = service.GetByName(teamName,mapper);
                if (res1.Count == 0)
                {
                    return Ok("No result found!");
                }
                return Ok(res1);
            }           

            return BadRequest("No teams available");
        }



        [HttpPost]

        public IActionResult Create(CreateTeamViewModel createTeamViewModel)
        {
            var map = mapper.Map<TeamDTO>(createTeamViewModel);

            var res = service.Create(map,mapper);
            if (res!=null)
            {
                return Ok(res);
            }
            return BadRequest("Can not create this team");            
        }
    }
}