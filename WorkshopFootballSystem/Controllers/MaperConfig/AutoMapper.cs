﻿using AutoMapper;
using FootballData;
using FootballData.Entities;
using FootBallServices.Models;
using System;
using System.Linq;
using WorkshopFootballSystem.ViewModels;

namespace WorkshopFootballSystem.MaperConfig
{
    public class AutoMapper : Profile
    {

        public AutoMapper()
        {

            CreateMap<CreatePlayerViewModel, CreatePlayerDTO>();

            CreateMap<CreateTeamViewModel, TeamDTO>();

            CreateMap<Team, TeamDTO>();//notFullyImplementedYet

            CreateMap<Player, PlayerDTO>().
                ForMember(dest => dest.FullName,
                opts => opts.MapFrom(src => src.FirstName + " " + src.LastName));

            CreateMap<PlayerDTO, CreatePlayerDTO>().
                ForMember(dest => dest.FirstName, ops => ops.MapFrom(src => src.FullName.Split()[0])).
                ForMember(dest => dest.LastName, ops => ops.MapFrom(src => src.FullName.Split()[1]));


            CreateMap<Tuple<Player, Data>, PlayerDTO>().ForMember(dest => dest.Id,
                opts => opts.MapFrom(src => src.Item1.Id)).
                ForMember(dest => dest.FullName,
                opts => opts.MapFrom(src => src.Item1.FirstName + " " + src.Item1.LastName)).
                ForMember(dest => dest.TeamName,
                opts => opts.MapFrom(src => src.Item2.Teams.FirstOrDefault(p => p.Id.Equals(src.Item1.TeamId)).Name)).
                ForMember(dest => dest.Goals,
                opts => opts.MapFrom(src => src.Item1.Goals));

            CreateMap<Team, TeamDTO>();

            CreateMap<Tuple<Team, Data>, TeamDTO>().
                ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Item1.Name)).
                ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Item1.Id)).
                ForMember(dest => dest.Players, opt => opt
                .MapFrom(src => src.Item2.Players.Count(p => p.TeamId.Equals(src.Item1.Id)))).
                ForMember(src=>src.Scored,opt=>opt.
                MapFrom(src=>src.Item2.Players.
                Where(p=>p.TeamId.
                Equals(src.Item1.Id)).Sum(p=>p.Goals)));

        }
    }
}
