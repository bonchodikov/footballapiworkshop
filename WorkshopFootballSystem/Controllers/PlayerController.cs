﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using FootBallServices.Contracts;
using FootBallServices.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WorkshopFootballSystem.ViewModels;

namespace WorkshopFootballSystem.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayerController : ControllerBase
    {
        IPlayerServices service;
        IMapper mapper;
        public PlayerController(IPlayerServices service, IMapper mapper)
        {
            this.service = service ?? throw new ArgumentNullException(nameof(service));
            this.mapper = mapper;
        }

        [HttpGet]
        
        public IActionResult GetAll()
        {
            var result = service.GetAll(mapper);
            if (result.Count != 0)
            {
                var str = new StringBuilder();
                foreach (var item in result)
                {
                    str.AppendLine(item.ToString());
                }
                return Ok(str.ToString());
            }
            return Ok("No players found");
        }

        [HttpGet]
        [Route("search")]
        public IActionResult SearchRes([FromQuery] string name, [FromQuery] int minplayers)
        {
            return null;
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(int id)
        {
            var result = service.GetById(id,mapper);
            if (result != null)
            {
                return Ok(result.ToString());
            }
            return BadRequest();
        }


        [HttpGet]
        [Route("search")]
        public IActionResult GetById([FromQuery] string firstName,[FromQuery] string lastName)
        {
            if (firstName!=null)
            {
                if (lastName!=null)
                {
                    var fullNameResult = service.GetByFullName(firstName, lastName, mapper);
                    if (fullNameResult != null)
                    {
                        var str = new StringBuilder();
                        foreach (var item in fullNameResult)
                        {
                            str.AppendLine(item.ToString());
                        }
                        return Ok(str.ToString());
                    }
                    return Ok("No matches found");

                }
                var result = service.GetByFirstName(firstName, mapper);
                if (result.Count != 0)
                {
                    var str = new StringBuilder();
                    foreach (var item in result)
                    {
                        str.AppendLine(item.ToString());
                    }
                    return Ok(str.ToString());
                }
                return BadRequest("No players found");

            }
            else if(lastName!=null)
            {
                var res = service.GetByLastName(lastName, mapper);
                if (res.Count!=0)
                {
                    var str = new StringBuilder();
                    foreach (var item in res)
                    {
                        str.AppendLine(item.ToString());
                    }
                    return Ok(str.ToString());
                }
                return Ok("No matches found !");
            }
            return BadRequest("Invalid search");
            
        }        

        [HttpPost]
        [Route("")]
        public IActionResult Create(CreatePlayerViewModel model)
        {

            var res = service.Create(mapper.Map<CreatePlayerDTO>(model),mapper);
            if (res!=null)
            {                
                return Ok(res.ToString());
            }
            return BadRequest();
        }






    }
}