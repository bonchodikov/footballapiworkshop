﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkshopFootballSystem.ViewModels
{
    public class CreateTeamViewModel
    {
        public string Name { get; set; }
    }
}
