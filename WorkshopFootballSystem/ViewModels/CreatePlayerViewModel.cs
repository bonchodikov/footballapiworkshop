﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WorkshopFootballSystem.ViewModels.Contracts;

namespace WorkshopFootballSystem.ViewModels
{
    public class CreatePlayerViewModel: ICreatePlayerVM
    {
        public int Goals { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? TeamId { get; set; }
    }
}
