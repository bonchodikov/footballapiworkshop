﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WorkshopFootballSystem.ViewModels.Contracts
{
    public interface ICreatePlayerVM
    {
         int Goals { get; set; }
         string FirstName { get; set; }
         string LastName { get; set; }
         int? TeamId { get; set; }
    }
}
