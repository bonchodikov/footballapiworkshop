﻿using FootballData.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace FootballData.Contracts
{
   public interface IData
    {
        DbSet<Player>  Players { get; set; }
        DbSet<Team> Teams { get; set; }       
    }
}
