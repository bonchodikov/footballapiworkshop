﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballData.Entities
{
   public class Player
    {
        public int Id { get; set; }
        public int Goals { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? TeamId { get; set; }
    }
}
