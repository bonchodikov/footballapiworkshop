﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FootballData.Entities
{
   public class Team
    {
        public Team()
        { }
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
