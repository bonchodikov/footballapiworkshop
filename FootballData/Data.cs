﻿using FootballData.Contracts;
using FootballData.Entities;
using FootballData.ModelsConfig;
using Microsoft.EntityFrameworkCore;

namespace FootballData
{
    public class Data: DbContext, IData
    {
        public Data(DbContextOptions<Data> options) :
            base(options)
        { }    

        public DbSet<Player> Players { get; set; }

        public DbSet<Team> Teams { get; set; }

        protected override void OnModelCreating(ModelBuilder model)
        {
            model.ApplyConfiguration(new PlayerConfig());
            model.ApplyConfiguration(new TeamConfig());
            base.OnModelCreating(model);
        }
    }
}
