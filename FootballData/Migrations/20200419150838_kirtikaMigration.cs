﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FootballData.Migrations
{
    public partial class kirtikaMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirsName",
                table: "Players");

            migrationBuilder.AddColumn<string>(
                name: "FirstName",
                table: "Players",
                maxLength: 20,
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstName",
                table: "Players");

            migrationBuilder.AddColumn<string>(
                name: "FirsName",
                table: "Players",
                type: "nvarchar(20)",
                maxLength: 20,
                nullable: true);
        }
    }
}
